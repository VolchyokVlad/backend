FROM ubuntu:16.04

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && apt-add-repository -y ppa:nginx/stable \
    && apt-get update \
    && apt-get install -y nginx \
    && apt-get install -y php7.0 \
    && apt-get install -y php7.0-fpm \
    && apt-get install -y supervisor \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /var/www/html \
    && mkdir -p /var/run/php

ADD default.conf /etc/nginx/conf.d/default.conf
COPY job1/ /var/www/html
ADD .env /var/www/.env
RUN chmod -R 777 /var/www/html

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

RUN rm /etc/php/7.0/fpm/pool.d/www.conf
ADD www.conf /etc/php/7.0/fpm/pool.d/www.conf

COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf
VOLUME ["/etc/supervisor/conf.d"]
WORKDIR /etc/supervisor/conf.d

EXPOSE 80 9000

CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]

